# color-tag

A module for distinctly coloring logging text

## Example usage

```js
const color = require('color-tag')

console.log(`
  ${color('what would you like for dinner?', '@mix')}
  ${color('pasta!', '@ziva')}
  ${color('spaghetti!', '@mix')}
`)
// =>
//  what would you like for dinner?     // colored green
//  pasta!                              // colored blue
//  spaghetti!                          // colored green
```

A random color is chosen based on a provided `id`.

## API

### `color(str, id) => coloredStr`

- `str` *String* - a piece of text you'd like colorized
- `id` *String|Number*
    - a unique identifer that will be used to map other related text to the same color
    - if `id` is ommitted, then `id = str` 


An example ommitting `id`:

```js
console.log(`
  ${color('@mix')}  | what would you like for dinner?
  ${color('@ziva')} | pasta!
  ${color('@mix')}  | spaghetti!
`)

