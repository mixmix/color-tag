const color = require('.')

const log = (str, id) => console.log(color(str, id))

log('[✓] ███ A')
log('[✓] ███ B')
log('[✓] ███ C')
log('[✓] ███ D')
log('[✓] ███ E')
log('[✓] ███ F')

log('[✓] A again (looped to beginning, allocate id)', 'color0')
log('[✓] and again (by id)', 'color0')

console.log()

// Example Usage 1
console.log(`
  ${color('what would you like for dinner?', '@mix')}
  ${color('pasta!', '@ziva')}
  ${color('spaghetti!', '@mix')}
`)

console.log()

// Example 2
console.log(`
  ${color('@mix')}  | what would you like for dinner?
  ${color('@ziva')} | pasta!
  ${color('@mix')}  | spaghetti!
`)
